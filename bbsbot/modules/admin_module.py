#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
"""
This Bot uses the Updater class to handle the bot.

First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)
from telegram import TelegramError, Chat, Message
from telegram import Update, Bot
from telegram.error import BadRequest
from telegram.ext import MessageHandler, Filters, CommandHandler
from telegram.ext.dispatcher import run_async

import bbsbot.modules.sql.users_sql as sql
from bbsbot import dispatcher, OWNER_ID, LOGGER
from bbsbot.modules.helper_funcs import locale
from bbsbot.modules.helper_funcs.filters import CustomFilters
import bbsbot.modules.sql.schemes_sql as schemes_sql

logger = logging.getLogger(__name__)

ADD_SCHEME, ADD_BOOK, CONFIRM = range(3)


@run_async
def start(bot: Bot, update: Update):
    user_id = update.effective_message.from_user.id
    update.effective_message.reply_text(
        locale.locale_dict[locale.get_locale(user_id)]["enter_scheme_name"]
    )
    return ADD_SCHEME


def begin_book_add(bot: Bot, update: Update, chat_data):
    user = update.effective_message.from_user
    user_id = update.effective_message.from_user.id
    msg_str = update.effective_message.text
    chat_data["scheme_name"] = msg_str
    chat_data["book"] = ""
    chat_data["book_view"] = ""
    update.effective_message.reply_text(
        locale.locale_dict[locale.get_locale(user_id)]["enter_books"].\
            format(msg_str))
    return ADD_BOOK


def continue_book_add(bot: Bot, update: Update, chat_data):
    user_id = update.effective_message.from_user.id
    msg_str = update.effective_message.text
    temp = schemes_sql.check_book_exists(msg_str)
    if temp:
        if chat_data["book"] != "":
            chat_data["book"] = chat_data["book"] + "," + str(temp)
            chat_data["book_view"] = chat_data["book_view"] + "," + msg_str
        else:
            chat_data["book"] = str(temp)
            chat_data["book_view"] = msg_str
        update.effective_message.reply_text(
            locale.locale_dict[locale.get_locale(user_id)]["book_entered"].format(msg_str))
        return ADD_BOOK
    else:
        update.effective_message.reply_text(
            locale.locale_dict[locale.get_locale(user_id)]["book_entered_fail"].format(msg_str))
        return ADD_BOOK


def book_done(bot: Bot, update: Update, chat_data):
    user_id = update.effective_message.from_user.id
    reply_keyboard = [['Yes', 'No']]
    if "book" in chat_data:
        update.message.reply_text(
            locale.locale_dict[locale.get_locale(user_id)]["book_entered_fail"].\
                format(chat_data["scheme_name"], chat_data["book_view"]),
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True, resize_keyboard=True)
        )
        return CONFIRM
    else:
        update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["no_books_entered"],
                                  reply_markup=ReplyKeyboardRemove())
        cancel(bot, update)
        return ConversationHandler.END


def confirm(bot: Bot, update: Update, chat_data):
    user_id = update.effective_message.from_user.id
    if update.effective_message.text == "Yes":
        if chat_data["book"] == "":
            update.effective_message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["no_books_entered"], reply_markup=ReplyKeyboardRemove())
            return ConversationHandler.END
        schemes_sql.insert_scheme(chat_data["scheme_name"], chat_data["book"])
        update.effective_message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["scheme_added"], reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END
    else:
        cancel(bot, update)
        return ConversationHandler.END


def cancel(bot: Bot, update: Update):
    user_id = update.effective_message.from_user.id
    update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["reg_scheme_again"],
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)

def print_books_abbrev_list(bot: Bot, update: Update):
    user_id = update.effective_message.from_user.id
    all_chapters = schemes_sql.get_all_chapters()
    return_message = ""
    for chapter in all_chapters:
        return_message += (chapter.book_chi + " : " + chapter.book_chi_s  + " : " + chapter.book_eng_s + "\n")
    update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["list_of_books"].format(return_message.strip()))


def get_active_schemes(bot: Bot, update: Update):
    user_id = update.effective_message.from_user.id
    reading_schemes = schemes_sql.get_all_active_schemes()
    if len(reading_schemes) == 0:
        update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["no_scheme_active"])
    else:
        return_message = ""
        for scheme in reading_schemes:
            return_message += (str(scheme.scheme_id) + " : " + scheme.description + " : Started " + str(scheme.start_date) + "\n")
        update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["list_of_schemes"].format(return_message.strip()))


def remove_scheme(bot: Bot, update: Update):
    user_id = update.effective_message.from_user.id
    effective_char = update.effective_message.entities[0]["length"]
    scheme_key = update.effective_message.text[effective_char:].strip()
    status = schemes_sql.deactivate_scheme(scheme_key)
    if not status:
        update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["scheme_delete"].format(scheme_key))
    else:
        update.message.reply_text(
            locale.locale_dict[locale.get_locale(user_id)]["scheme_not_found"].format(scheme_key))


__help__ = "admin help"  # no help string
__mod_name__ = "Add Reading Schemes"

CONV_HANDLER = ConversationHandler(
    entry_points=[CommandHandler('reg', start, filters=CustomFilters.sudo_filter)],
    states={
        ADD_SCHEME: [MessageHandler(Filters.text, begin_book_add, pass_chat_data=True)],
        ADD_BOOK: [MessageHandler(Filters.text, continue_book_add, pass_chat_data=True),
                   CommandHandler('done', book_done, pass_chat_data=True)],
        CONFIRM: [RegexHandler('^(Yes|No)$', confirm, pass_chat_data=True)]
    },
    per_user=True,
    fallbacks=[CommandHandler('cancel', cancel)]
)

BOOK_ABBREVIATION_HANDLER = CommandHandler('list', print_books_abbrev_list, filters=CustomFilters.sudo_filter)
ACTIVE_SCHEME_HANDLER = CommandHandler('active', get_active_schemes, filters=CustomFilters.sudo_filter)
REMOVE_SCHEME_HANDLER = CommandHandler('remove', remove_scheme, filters=CustomFilters.sudo_filter)

dispatcher.add_handler(CONV_HANDLER)
dispatcher.add_error_handler(error)
dispatcher.add_handler(BOOK_ABBREVIATION_HANDLER)
dispatcher.add_handler(ACTIVE_SCHEME_HANDLER)
dispatcher.add_handler(REMOVE_SCHEME_HANDLER)
