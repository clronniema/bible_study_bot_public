import threading
import datetime
from sqlalchemy import Column, Integer, UnicodeText, Date, Boolean, ForeignKey
from sqlalchemy import or_, func

from bbsbot.modules.sql import SESSION, BASE


class ReadingSchemes(BASE):
    __tablename__ = "reading_scheme"
    scheme_id = Column(Integer, primary_key=True, autoincrement=True)
    start_date = Column(Date, default=func.now(), nullable=False)
    description = Column(UnicodeText)
    active = Column(Boolean)

    def __init__(self, description, active):
        self.description = description
        self.active = active

    def __repr__(self):
        return "<User info %d>" % self.user_id


class Book(BASE):
    __tablename__ = "scheme_book"
    book_id = Column(Integer, primary_key=True)
    book_chi = Column(UnicodeText)
    book_chi_s = Column(UnicodeText)
    book_eng = Column(UnicodeText)
    book_eng_s = Column(UnicodeText)
    chapters = Column(Integer)
    verses = Column(Integer)

    def __init__(self, book_no,book_chi,book_chi_s,book_eng,book_eng_s,chapters,verses):
        self.book_no = book_no
        self.book_chi = book_chi
        self.book_chi_s = book_chi_s
        self.book_eng = book_eng
        self.book_eng_s = book_eng_s
        self.chapters = chapters
        self.verses = verses

    def __repr__(self):
        return "<Book name %d>" % self.book_name


class SchemeBookMap(BASE):
    __tablename__ = "scheme_book_map"
    scheme_book_map_id = Column(Integer, primary_key=True, autoincrement=True)
    scheme_id = Column(Integer, ForeignKey(ReadingSchemes.scheme_id), nullable=False)
    book_id = Column(Integer, ForeignKey(Book.book_id), nullable=False)

    def __init__(self, scheme_id, book_id):
        self.scheme_id = scheme_id
        self.book_id = book_id

    def __repr__(self):
        return "<User info %d>" % self.user_id

ReadingSchemes.__table__.create(checkfirst=True)
SchemeBookMap.__table__.create(checkfirst=True)
Book.__table__.create(checkfirst=True)

INSERTION_LOCK = threading.RLock()


def add_new_schemes(book_name):
    with INSERTION_LOCK:
        book_add = SESSION.query(Book).get(book_name)
        if not book_add:
            book_add = Book(book_name)
            SESSION.add(book_add)
            SESSION.commit()
            return
        else:
            SESSION.close()
            return None


def get_all_active_schemes():
    try:
        return SESSION.query(ReadingSchemes).filter(ReadingSchemes.active == True).order_by(ReadingSchemes.description.asc()).all()
    finally:
        SESSION.close()


def get_all_chapters():
    try:
        return SESSION.query(Book).order_by(Book.book_id).all()
    finally:
        SESSION.close()


def insert_scheme(scheme_name, scheme_books):
    with INSERTION_LOCK:
        scheme = ReadingSchemes(description=scheme_name, active=True)
        SESSION.add(scheme)
        SESSION.flush()
        scheme_key = scheme.scheme_id
        for book in scheme_books.split(','):
            scheme_book_map = SchemeBookMap(scheme_id=scheme_key, book_id=book)
            SESSION.add(scheme_book_map)
        SESSION.commit()
        return


def deactivate_scheme(key):
    with INSERTION_LOCK:
        scheme = SESSION.query(ReadingSchemes).\
            filter(ReadingSchemes.active == True, ReadingSchemes.scheme_id == key)
        if not scheme:
            return False
        else:
            scheme.active = False
            SESSION.commit()
            return scheme.desc


def check_book_exists(book_name):
    book = SESSION.query(Book).filter(
        or_(Book.book_chi_s == book_name,
            func.lower(Book.book_eng_s) == func.lower(book_name))).first()
    if book:
        SESSION.close()
        return book.book_id
