import threading
from sqlalchemy import Column, Integer, UnicodeText, Date, Boolean
from sqlalchemy import or_, func, and_, join
from bbsbot.modules.sql import SESSION, BASE
import _datetime as datetime
import bbsbot.modules.sql.schemes_sql as schemes_sql


class SchemeCheckIn(BASE):
    __tablename__ = "scheme_check_in"
    check_in_id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer)
    scheme_book_map_id = Column(Integer)
    check_in_date = Column(Date, server_default=func.now())
    chapters = Column(Integer)

    def __init__(self, user_id, scheme_book_map_id, chapters):
        self.user_id = user_id
        self.scheme_book_map_id = scheme_book_map_id
        self.chapters = chapters

    def __repr__(self):
        return "<User info %d>" % self.user_id


SchemeCheckIn.__table__.create(checkfirst=True)

INSERTION_LOCK = threading.RLock()


def check_in(user_id, scheme_book_map_id, chapters):
    scheme_check_in = SESSION.query(SchemeCheckIn).\
        filter(SchemeCheckIn.user_id == user_id,
                    SchemeCheckIn.scheme_book_map_id == scheme_book_map_id,
                    SchemeCheckIn.check_in_date == datetime.date.today()
               )
    if scheme_check_in.count() == 0:
        scheme_check_in = SchemeCheckIn(user_id, scheme_book_map_id, chapters)
        SESSION.add(scheme_check_in)
        SESSION.commit()
        return
    else:
        scheme_check_in.chapters = chapters
        SESSION.commit()
        SESSION.close()
        return None


def get_scheme_check_in(user_id):
    return None


def get_today_check_in(user_id):
    return None
