import datetime
import threading
from sqlalchemy import Column, Integer, UnicodeText, Date, Boolean, ForeignKey
from sqlalchemy import or_, func, and_, join
from bbsbot.modules.sql import SESSION, BASE, schemes_sql
from bbsbot.modules.sql.schemes_sql import ReadingSchemes


class SchemeRegistration(BASE):
    __tablename__ = "scheme_registration"
    reg_id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer)
    scheme_id = Column(Integer, ForeignKey(ReadingSchemes.scheme_id), nullable=False)

    def __init__(self, user_id, scheme_id):
        self.user_id = user_id
        self.scheme_id = scheme_id

    def __repr__(self):
        return "<User info %d>" % self.user_id


SchemeRegistration.__table__.create(checkfirst=True)
INSERTION_LOCK = threading.RLock()


def register_user_to_scheme(user_id, scheme_id):
    reg_id = SESSION.query(SchemeRegistration).\
        filter(SchemeRegistration.scheme_id == scheme_id,
               SchemeRegistration.user_id == user_id)
    if reg_id.count() == 0:
        scheme_registration = SchemeRegistration(user_id, scheme_id)
        SESSION.add(scheme_registration)
        SESSION.commit()
        return True
    else:
        return False

def register(user_id, scheme_id, book_id):
    scheme_book_map_id = SESSION.query(schemes_sql.SchemeBookMap).\
        join(SchemeRegistration.scheme_book_map_id).\
        filter(schemes_sql.SchemeBookMap.scheme_id == scheme_id,
                    schemes_sql.SchemeBookMap.book_id == book_id,
                    SchemeRegistration.user_id == user_id
               )
    if not scheme_book_map_id:
        scheme_book_map_id = SESSION.query(schemes_sql.SchemeBookMap).\
            join(SchemeRegistration.scheme_book_map_id).\
            filter(schemes_sql.SchemeBookMap.scheme_id == scheme_id,
                             schemes_sql.SchemeBookMap.book_id == book_id
                   )
        scheme_registration = SchemeRegistration(user_id, scheme_book_map_id)
        SESSION.add(scheme_registration)
        SESSION.commit()
        return
    else:
        SESSION.close()
        return None


def unregister(user_id, scheme_id, book_id):
    scheme_book_map_id = SESSION.query(schemes_sql.SchemeBookMap).\
        join(SchemeRegistration.scheme_book_map_id).\
        filter(schemes_sql.SchemeBookMap.scheme_id == scheme_id,
                    schemes_sql.SchemeBookMap.book_id == book_id,
                    SchemeRegistration.user_id == user_id
               )
    if scheme_book_map_id:
        scheme_registered = SESSION.query(SchemeRegistration).\
            filter(SchemeRegistration.scheme_book_map_id==scheme_book_map_id)
        SESSION.remove(scheme_registered)
        SESSION.commit()
        return
    else:
        SESSION.close()
        return None


def view_user_reg_schemes(user_id):
    schemes_registered = SESSION.query(SchemeRegistration, schemes_sql.ReadingSchemes).\
        filter(SchemeRegistration.user_id == user_id,
                    schemes_sql.ReadingSchemes.active,
                    schemes_sql.ReadingSchemes.scheme_id == SchemeRegistration.scheme_id
               )
    if schemes_registered:
        return schemes_registered
    else:
        return None


def view_registered_schemes(user_id):
    schemes_registered = SESSION.query(SchemeRegistration).\
        join(schemes_sql.SchemeBookMap.scheme_book_map_id).\
        join(schemes_sql.ReadingSchemes.scheme_id).\
        filter(and_(SchemeRegistration.user_id==user_id,
                    schemes_sql.ReadingSchemes.active))
    if schemes_registered:
        return schemes_registered
    else:
        return None


def view_registered_scheme_books(user_id):
    books_registered = SESSION.query(SchemeRegistration).\
        join(schemes_sql.SchemeBookMap.scheme_book_map_id).\
        join(and_(schemes_sql.ReadingSchemes.scheme_id),
             schemes_sql.Book.book_id).\
        filter(and_(SchemeRegistration.user_id==user_id,
                    schemes_sql.ReadingSchemes.active))
    if books_registered:
        return books_registered
    else:
        return None


def get_scheme_books(scheme_id):
    books_available = SESSION.query(schemes_sql.SchemeBookMap, schemes_sql.Book).\
        filter(schemes_sql.SchemeBookMap.scheme_id == int(scheme_id),
               schemes_sql.SchemeBookMap.book_id == schemes_sql.Book.book_id
               )
    if books_available:
        return books_available
    else:
        return None


def view_all_registered():
        return None


