import logging

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardButton, InlineKeyboardMarkup)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler, CallbackQueryHandler)
from telegram import Update, Bot
from telegram.ext import MessageHandler, Filters, CommandHandler
from telegram.ext.dispatcher import run_async

from bbsbot import dispatcher
from bbsbot.modules.helper_funcs import locale
from bbsbot.modules.helper_funcs.filters import CustomFilters

import bbsbot.modules.sql.scheme_check_in_sql as check_in_sql
import bbsbot.modules.sql.schemes_sql as schemes_sql
import bbsbot.modules.sql.scheme_reg_sql as schemes_reg_sql
import bbsbot.modules.sql.scheme_check_in_sql as scheme_check_in_sql

import bbsbot.modules.helper_funcs.build_menu as menu_builder

logger = logging.getLogger(__name__)

SCHEME_CHOOSE, BOOK_CHOOSE, CHAPTERS = range(3)
SCHEME_CHOOSE = range(1)

#==============================

@run_async
def registration_start(bot: Bot, update: Update):
    user_id = update.effective_message.from_user.id
    schemes = schemes_sql.get_all_active_schemes()
    scheme_dict = {}
    for scheme in schemes:
        scheme_dict[scheme.description] = scheme.scheme_id
    reply_markup = menu_builder.build_reply_markup(scheme_dict)
    update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["select_scheme"], reply_markup=reply_markup)
    return SCHEME_CHOOSE


def save_scheme(bot: Bot, update: Update):
    user_id = update.effective_message.chat.id
    query = update.callback_query
    scheme_id = query.data
    scheme_result = schemes_reg_sql.register_user_to_scheme(user_id, scheme_id)
    if scheme_result:
        bot.edit_message_text(
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
            text=locale.locale_dict[locale.get_locale(user_id)]["scheme_joined"]
        )
        #bot.send_message(update.callback_query.from_user.id, "Scheme joined", reply_markup=ReplyKeyboardRemove())
    else:
        bot.edit_message_text(
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
            text=locale.locale_dict[locale.get_locale(user_id)]["scheme_already_joined"]
        )
        #bot.send_message(update.callback_query.from_user.id, "Scheme already joined", reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def cancel_join(bot: Bot, update: Update):
    user_id = update.effective_message.chat.id
    update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["scheme_cancel"],
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END




##==============================

@run_async
def check_in_start(bot: Bot, update: Update):
    # /c checkin start
    # create list for SCHEME_CHOOSE
    # listen for scheme choose and return books
    user_id = update.effective_message.chat.id
    schemes = schemes_reg_sql.view_user_reg_schemes(update.effective_message.chat.id)
    if schemes.count() == 0:
        update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["no_scheme"])
        return ConversationHandler.END
    scheme_dict = {}
    for scheme in schemes:
        scheme_dict[scheme.ReadingSchemes.description] = scheme.ReadingSchemes.scheme_id
    reply_markup = menu_builder.build_reply_markup(scheme_dict)
    update.message.reply_text(
        locale.locale_dict[locale.get_locale(user_id)]["select_scheme"], reply_markup=reply_markup)
    return SCHEME_CHOOSE


def scheme_chosen(bot: Bot, update: Update, chat_data):
    #TODO
    # enter number of chapters
    user_id = update.effective_message.chat.id
    query = update.callback_query
    scheme_id = query.data
    chat_data["scheme_id"] = scheme_id

    books = schemes_reg_sql.get_scheme_books(scheme_id)
    scheme_book_dict = {}
    for book in books:
        scheme_book_dict[book.Book.book_chi + ' ' + book.Book.book_eng] = book.SchemeBookMap.scheme_book_map_id
    reply_markup = menu_builder.build_reply_markup(scheme_book_dict)

    bot.edit_message_text(
        chat_id=query.message.chat_id,
        message_id=query.message.message_id,
        text=locale.locale_dict[locale.get_locale(query.message.chat_id)]["select_book"],
        reply_markup=reply_markup
    )
    return BOOK_CHOOSE


def book_chosen(bot: Bot, update: Update, chat_data):
    #TODO
    # enter number of chapters
    user_id = update.effective_message.chat.id
    query = update.callback_query
    scheme_book_map_id = query.data

    chat_data["scheme_book_map_id"] = scheme_book_map_id
    bot.edit_message_text(
        chat_id=query.message.chat_id,
        message_id=query.message.message_id,
        text=locale.locale_dict[locale.get_locale(query.message.chat_id)]["num_books"]
    )
    return CHAPTERS


def books_entered(bot: Bot, update: Update, chat_data):
    # check if already checked in
    # overwrite / save
    user_id = update.effective_message.chat.id
    try:
        myInput = int(update.effective_message.text)
        chat_data["chapters"] = myInput
    except Exception:
        return CHAPTERS

    scheme_book_map_id = chat_data["scheme_book_map_id"]
    chapters = chat_data["chapters"]

    #schemes_sql.insert_scheme(chat_data["scheme_name"], chat_data["book"])
    check_in_sql.check_in(user_id, scheme_book_map_id, chapters)
    update.effective_message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["chapters_read"].\
                                        format(chat_data["chapters"]))


    check_in_sql.check_in(user_id, scheme_book_map_id, myInput)
    update.effective_message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["signed_in"].\
                                        format(chat_data["scheme_id"], chat_data["book_name"], chat_data["chapters"]),
                                        reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def cancel(bot: Bot, update: Update):
    user_id = update.effective_message.chat.id
    update.message.reply_text(locale.locale_dict[locale.get_locale(user_id)]["check_in_again"],
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def get_today_check_in(bot: Bot, update: Update):
    #TODO get today's check in
    scheme_check_in_sql.get_today_check_in(update.effective_message.chat.id)
    return


def get_scheme_check_in(bot: Bot, update: Update):
    #TODO get scheme check in
    scheme_check_in_sql.get_scheme_check_in(update.effective_message.chat.id)
    return


__help__ = "admin help"  # no help string
__mod_name__ = "Register and Check In"



REGISTRATION_HANDLER = ConversationHandler(
    entry_points=[CommandHandler('join', registration_start)],
    states={
        SCHEME_CHOOSE: [CallbackQueryHandler(save_scheme)],
    },
    per_user=True,
    fallbacks=[CommandHandler('cancel', cancel_join)]
)


CONV_HANDLER = ConversationHandler(
    entry_points=[CommandHandler('sign', check_in_start, filters=CustomFilters.sudo_filter)],
    states={
        SCHEME_CHOOSE: [CallbackQueryHandler(scheme_chosen, pass_chat_data=True)],
        BOOK_CHOOSE: [CallbackQueryHandler(book_chosen, pass_chat_data=True)],
        CHAPTERS: [MessageHandler(Filters.text, books_entered, pass_chat_data=True)]
    },
    per_user=True,
    fallbacks=[CommandHandler('cancel', cancel)]
)

#TODAY_CHECKIN_HANDLER = CommandHandler('today', get_today_check_in, filters=CustomFilters.sudo_filter)
#SCHEME_CHEKCIN_HANDLER = CommandHandler('history', get_scheme_check_in, filters=CustomFilters.sudo_filter)

dispatcher.add_handler(REGISTRATION_HANDLER)
dispatcher.add_handler(CONV_HANDLER)
dispatcher.add_error_handler(error)
#dispatcher.add_handler(TODAY_CHECKIN_HANDLER)
#dispatcher.add_handler(SCHEME_CHEKCIN_HANDLER)


