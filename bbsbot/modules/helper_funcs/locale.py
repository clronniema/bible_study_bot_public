from telegram.ext.dispatcher import run_async
import bbsbot.modules.sql.users_sql as sql
from bbsbot.modules.sql import BASE, SESSION


class Locale:

    locale_dict = {}

    eng_dict = {
        'broadcast': 'Broadcast complete. {} groups failed to receive the message, probably due to being kicked.',
        'nickname': "nickname updated as {}",
        'enter_scheme_name': "Enter scheme name",
        'enter_books': "Scheme name: {0}\nAdd books below, i.e. Genesis. Or type /done to complete.",
        'book_entered': "Book name: {0}\nAdd books below, i.e. Genesis. Or type /done to complete.",
        'book_entered_failed': "Book name: {0}\nAdd books below, i.e. Genesis. Or type /done to complete.",
        'scheme_confirm': "Book name: {0}\nAdd books below, i.e. Genesis. Or type /done to complete.",
        'no_books_reg': "No books registered",
        'no_books_entered': "No books entered",
        'scheme_added': "Scheme added",
        'reg_scheme_again': "Enter /reg to register new schemes again",
        'no_scheme_active': "No scheme active",
        'list_of_books': "Below is the list of abbreviations of Bible books:'\n{0}",
        'list_of_schemes': "Below is the list of abbreviations of Bible chapters:'\n{0}",
        'scheme_delete': "Scheme {0} is deleted",
        'scheme_not_found': "Scheme {0} not found",

        'select_scheme': "Select scheme",
        'scheme_joined': "Scheme joined",
        'scheme_already_joined': "Scheme already joined",
        'scheme_cancel': "Enter /join to register in a scheme again",
        'no_scheme': "No schemes active",
        'select_book': "Select book read",
        'num_books': "Enter number of books read",
        'chapters_read': "Signed in no of chaptes: {0}",
        'signed_in': "Signed in: {0}, Book: {1}, Chapters: {2}",
        'check_in_again': 'Enter /c to check in again'
    }



    chi_dict = {}

    locale_dict.update(eng_dict)
    locale_dict.update(chi_dict)


    @run_async
    def get_locale(user_id):
        user = SESSION.query(sql.Users).get(user_id)
        if not user:
            return "zh"
        else:
            return user.locale



#user_id = update.effective_message.from_user.id
#locale.locale_dict[locale.get_locale(user_id)]["nickname"]
