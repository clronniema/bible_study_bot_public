from bbsbot import dispatcher

PM_START_TEXT = """
Hi {}, my name is {}! I'm a group manager bot maintained by [RP](tg://user?id={}).
I'm built using the python-telegram-bot library and is loosely based on [PaulSonOfLars/tgbot](github.com/PaulSonOfLars/tgbot)

You can find the list of available commands with /help.
"""

HELP_STRINGS = """
Hey there! My name is *{}*. Have a look below for a list of my functions.

*Main* commands available:
 - /help: PM's you this message.
 - /help <module name>: PM's you info about that module.

""".format(dispatcher.bot.first_name)
