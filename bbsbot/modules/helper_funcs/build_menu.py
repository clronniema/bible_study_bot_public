from telegram import (InlineKeyboardButton, InlineKeyboardMarkup)

def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu

def build_reply_markup(button_dict, n_cols=2):
    button_list = []
    for key, val in button_dict.items():
        button_list.append(InlineKeyboardButton(key, callback_data=val))
    return InlineKeyboardMarkup(build_menu(button_list, n_cols=n_cols))
    #bot.send_message("msg", "A two-column menu", reply_markup = reply_markup)